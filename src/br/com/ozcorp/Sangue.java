package br.com.ozcorp;

/**
 * Este enum guarda os tipos sanguineos, utilizados pela classe funcionário.
 * 
 * @author Samantha Gomes Costa
 *
 */
public enum Sangue {
	APOS("A+"), ANEG("A-"), BPOS("B+"), BNEG("B-"), ABPOS("AB+"), ABNEG("AB-"), OPOS("O+"), ONEG("O-");
	
	public String sangue;
	
	/**
	 * Transforma em String
	 * 
	 * @param sangue tipo sanguineo
	 */
	Sangue(String sangue){
		this.sangue = sangue;
	}
}
