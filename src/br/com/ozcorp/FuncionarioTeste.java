package br.com.ozcorp;

/**
 * 
 * @author Samantha Gomes Costa
 *
 */
public class FuncionarioTeste {
	
	public static void main(String[] args) {
	
		Funcionario a = new Funcionario("Alice", "4685355-X", "684257985-16", "24-C", "alicealmeida@senai.sp",
				new Cargo("Gerente", 110_700, "Gerencia", "Gr"), "salva_nois", Sangue.ABNEG, Sexo.FEMININO, NivelAcesso.GERENTE);
		
		System.out.println("\t   DADOS");
		System.out.println("FUNCIONÁRIO	" + a.getNome());
		System.out.println("RG		" + a.getRg());
		System.out.println("CPF		" + a.getCpf());
		System.out.println("MATRÍCULA	" + a.getMatricula());
		System.out.println("E-MAIL		" + a.getEmail());
		System.out.println("SENHA		" + a.getSenha());
		System.out.println("CARGO		" + a.getCargo().getCargo());
		System.out.println("SALÁRIO		" + a.getCargo().getSalario());
		System.out.println("DEPARTAMENTO	" + a.getCargo().getDepartamento() + "	SIGLA: " + a.getCargo().getSigla());
		System.out.println("SANGUE		" + a.getSangue().sangue);
		System.out.println("SEXO		" + a.getSexo().sexo);
		//System.out.println("Acesso		" + a.getAcesso());
	
	}	
}
