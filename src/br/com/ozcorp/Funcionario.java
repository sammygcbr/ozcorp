package br.com.ozcorp;

/**
 * 
 * @author Samantha Gomes Costa
 *
 */
public class Funcionario {
	
	//atributos
	private String nome;
	private String rg;
	private String cpf;
	private String matricula;
	private String email;
	private Cargo cargo;
	private String senha;
	private Sangue sangue;
	private Sexo sexo;
	private NivelAcesso acesso;
	
		
	//construtor
	public Funcionario(String nome, String rg, String cpf, String matricula, String email, Cargo cargo, String senha,
			Sangue sangue, Sexo sexo, NivelAcesso acesso) {
		super();
		this.nome = nome;
		this.rg = rg;
		this.cpf = cpf;
		this.matricula = matricula;
		this.email = email;
		this.cargo = cargo;
		this.senha = senha;
		this.sangue = sangue;
		this.sexo = sexo;
		this.acesso = acesso;
	}

	//getters & setters
	public String getNome() {
		return nome;
	}


	public void setNome(String nome) {
		this.nome = nome;
	}


	public String getRg() {
		return rg;
	}


	public void setRg(String rg) {
		this.rg = rg;
	}


	public String getCpf() {
		return cpf;
	}


	public void setCpf(String cpf) {
		this.cpf = cpf;
	}


	public String getMatricula() {
		return matricula;
	}


	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}


	public String getEmail() {
		return email;
	}


	public void setEmail(String email) {
		this.email = email;
	}


	public Cargo getCargo() {
		return cargo;
	}


	public void setCargo(Cargo cargo) {
		this.cargo = cargo;
	}


	public String getSenha() {
		return senha;
	}


	public void setSenha(String senha) {
		this.senha = senha;
	}


	public Sangue getSangue() {
		return sangue;
	}


	public void setSangue(Sangue sangue) {
		this.sangue = sangue;
	}


	public Sexo getSexo() {
		return sexo;
	}


	public void setSexo(Sexo sexo) {
		this.sexo = sexo;
	}


	public NivelAcesso getAcesso() {
		return acesso;
	}


	public void setAcesso(NivelAcesso acesso) {
		this.acesso = acesso;
	}
	
}