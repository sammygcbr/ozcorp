package br.com.ozcorp;

/**
 * 
 * @author Samantha Gomes Costa
 *
 */
public class Cargo {

	//atributos
	private String cargo;
	private double salario;
	private String departamento;
	private String sigla;
	
	//construtor
	public Cargo(String cargo, double salario, String departamento, String sigla) {
		super();
		this.cargo = cargo;
		this.salario = salario;
		this.departamento = departamento;
		this.sigla = sigla;
	}

	//getters & setters
	public String getCargo() {
		return cargo;
	}

	public void setCargo(String cargo) {
		this.cargo = cargo;
	}

	public double getSalario() {
		return salario;
	}

	public void setSalario(double salario) {
		this.salario = salario;
	}

	public String getDepartamento() {
		return departamento;
	}

	public void setDepartamento(String departamento) {
		this.departamento = departamento;
	}
	
	public String getSigla() {
		return sigla;
	}

	public void setSigla(String sigla) {
		this.sigla = sigla;
	}
	
	
	
}
