package br.com.ozcorp;

/**
 * Este enum guarda os generos sexuais, utilizados pela classe funcionário.
 * 
 * @author Samantha Gomes Costa
 *
 */
public enum Sexo {
	MASCULINO("Masculino"), FEMININO("Feminino"), OUTROS("Outro");
	
	public String sexo;
	
	/**
	 * Transforma em String
	 * 
	 * @param sexo genero sexual
	 */
	Sexo(String sexo){
		this.sexo = sexo;
	}
}
