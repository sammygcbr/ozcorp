package br.com.ozcorp;

/**
 * Este enum guarda os niveis de acesso, utilizados pela classe funcionário.
 * 
 * @author Samantha Gomes Costa
 *
 */
public enum NivelAcesso {
	DIRETORIA("0"), SECRETARIA("1"), GERENTE("2"), ENGENHEIRO("3"), ANALISTA("4");
	
	public String acesso;
	
	/**
	 * Transforma em String
	 * 
	 * @param acesso cargo
	 */
	NivelAcesso(String acesso){
		this.acesso = acesso;
	}
}
